package com.example.tubes_03.view;

public interface FragmentListener {
    void changePage(int page);
}
